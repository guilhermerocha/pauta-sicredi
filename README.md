# pauta-sicredi

No cooperativismo, cada associado possui um voto e as decisões são tomadas em assembleias,
por votação. Imagine que você deve criar uma solução backend para gerenciar essas sessões de
votação.

Essa solução deve ser executada na nuvem e promover as seguintes funcionalidades através de
uma API REST:

- Cadastrar uma nova pauta
- Abrir uma sessão de votação em uma pauta (a sessão de votação deve ficar aberta por um tempo determinado na chamada de abertura ou 1 minuto por default)
- Receber votos dos associados em pautas (os votos são apenas 'Sim'/'Não'. Cada associado é identificado por um id único e pode votar apenas uma vez por pauta)
- Contabilizar os votos e dar o resultado da votação na pauta

## Stack 

- Java 11
- Spring
- Maven
- MongoDB
- Swagger
- JUnit
- Docker

## Requisitos
- Maven 3.6+
- Java 11
- Docker 

## Rodar a aplicação

Com o docker rodando na máquina faça o clone do projeto e vá até a raiz utilizando o terminal de comando e executar os seguintes comandos:

- mvn clean install -D maven.test.skip=true
- docker build -t springboot-mongodb:1.0 .
- cd src\main\resources docker-compose up

## Acessar a API

A Aplicação ficará disponível nas URLs abaixo.
- http://localhost:8080/

Documentação:
- http://localhost:8080/swagger-ui.html

#### Tarefa Bônus 1 - Integração com sistemas externos
    Serviço para chamar a url externa foi implementado no serviço 'IntegrationService'(Incompleto).

#### Tarefa Bônus 2 - Mensageria e filas
    Não foi feito.

#### Tarefa Bônus 3 - Performance
    Não foi feito.

#### Tarefa Bônus 4 - Versionamento da API
    Utilizei o workflow para versionar o código em features respeitando cada funcionalidade dentro do sistema.