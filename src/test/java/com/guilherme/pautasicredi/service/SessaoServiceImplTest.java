package com.guilherme.pautasicredi.service;

import com.guilherme.pautasicredi.dto.AbrirSessaoDTO;
import com.guilherme.pautasicredi.dto.SessaoDTO;
import com.guilherme.pautasicredi.model.Pauta;
import com.guilherme.pautasicredi.model.Sessao;
import com.guilherme.pautasicredi.repository.PautaRepository;
import com.guilherme.pautasicredi.repository.SessaoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class SessaoServiceImplTest {

    @InjectMocks
    private SessaoServiceImpl service;

    @Mock
    private SessaoRepository sessaoRepository;

    @Mock
    private PautaRepository pautaRepository;

    @Mock
    private Environment environment;

    @BeforeEach
    void setUp() {
    }

    @Test
    void criarUmaSessaoDeVotacao() {
        AbrirSessaoDTO abrirSessaoDTO = new AbrirSessaoDTO();
        abrirSessaoDTO.setIdPauta("567cwu4902");
        abrirSessaoDTO.setMinutosLimite(0);
        Pauta pauta = new Pauta();
        pauta.setNome("Sicredi teste");
        pauta.setId(abrirSessaoDTO.getIdPauta());
        when(this.pautaRepository.findById(abrirSessaoDTO.getIdPauta())).thenReturn(Optional.of(pauta));
        when(this.environment.getProperty("default.limite.minutos")).thenReturn("1");

        SessaoDTO retorno = this.service.criarUmaSessaoDeVotacao(abrirSessaoDTO);
        assertEquals("Sicredi teste", retorno.getPauta().getNome());
        assertEquals(1, retorno.getMinutosLimite());
    }

    @Test
    void obterListaDeSessoesParaVotar() {
        this.treinamentoObterSessoes();
        List<SessaoDTO> retorno = this.service.obterListaDeSessoesParaVotar();

        assertEquals(1, retorno.size());
        SessaoDTO dto = retorno.get(0);
        assertNotNull( dto.getPauta());
        assertEquals(1, dto.getMinutosLimite());
        assertEquals("Sicredi teste", dto.getPauta().getNome());

    }

    private void treinamentoObterSessoes() {
        List<Sessao> sessoes = this.criarSessoes();
        when(this.sessaoRepository.findAll()).thenReturn(sessoes);
    }

    private List<Sessao> criarSessoes() {
        List<Sessao> sessoes = new ArrayList<>();
        Sessao sessao = new Sessao();
        sessao.setMinutosLimite(1);
        Pauta pauta = new Pauta();
        pauta.setNome("Sicredi teste");
        sessao.setPauta(pauta);
        sessoes.add(sessao);
        return sessoes;
    }

    @Test
    void obterSessoesPorID() {

    }

    @Test
    void obterSessao() {
    }

    @Test
    void obtemResultadoVotacao() {
    }

    @Test
    void votarNaPauta() {
    }
}