package com.guilherme.pautasicredi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PautaSicrediApplication {

	public static void main(String[] args) {
		SpringApplication.run(PautaSicrediApplication.class, args);
	}

}
