package com.guilherme.pautasicredi.dto;

public class SessaoResultadoDTO {
    private PautaDTO pauta;
    private Long votoSim;
    private Long votoNao;

    public SessaoResultadoDTO(long votoSim, long votoNao, PautaDTO pauta) {
        this.votoSim = votoSim;
        this.votoNao = votoNao;
        this.pauta = pauta;
    }

    public PautaDTO getPauta() {
        return pauta;
    }

    public void setPauta(PautaDTO pauta) {
        this.pauta = pauta;
    }

    public Long getVotoSim() {
        return votoSim;
    }

    public void setVotoSim(Long votoSim) {
        this.votoSim = votoSim;
    }

    public Long getVotoNao() {
        return votoNao;
    }

    public void setVotoNao(Long votoNao) {
        this.votoNao = votoNao;
    }
}
