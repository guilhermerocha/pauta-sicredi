package com.guilherme.pautasicredi.dto;

import com.guilherme.pautasicredi.model.Resposta;

public class VotoDTO {

    private String sessaoId;
    private String cpf;
    private Resposta resposta;

    public String getSessaoId() {
        return sessaoId;
    }

    public void setSessaoId(String sessaoId) {
        this.sessaoId = sessaoId;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Resposta getResposta() {
        return resposta;
    }

    public void setResposta(Resposta resposta) {
        this.resposta = resposta;
    }
}
