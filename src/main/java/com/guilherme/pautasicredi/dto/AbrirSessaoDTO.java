package com.guilherme.pautasicredi.dto;

public class AbrirSessaoDTO {
    private String idPauta;
    private Integer minutosLimite;

    public String getIdPauta() {
        return idPauta;
    }

    public void setIdPauta(String idPauta) {
        this.idPauta = idPauta;
    }

    public Integer getMinutosLimite() {
        return minutosLimite;
    }

    public void setMinutosLimite(Integer minutosLimite) {
        this.minutosLimite = minutosLimite;
    }
}
