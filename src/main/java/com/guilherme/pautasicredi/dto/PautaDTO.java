package com.guilherme.pautasicredi.dto;

import org.bson.types.ObjectId;

public class PautaDTO {

    private String id;
    private String nome;

    public PautaDTO() {
    }

    public String getNome() {
        return nome;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
