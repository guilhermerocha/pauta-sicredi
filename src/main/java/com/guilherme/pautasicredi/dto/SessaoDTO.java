package com.guilherme.pautasicredi.dto;

import com.guilherme.pautasicredi.model.Pauta;
import com.guilherme.pautasicredi.model.Voto;
import org.springframework.data.annotation.Id;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;

public class SessaoDTO {

    private String id;
    private PautaDTO pauta;
    private Integer minutosLimite;
    private LocalDateTime dataLimite;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PautaDTO getPauta() {
        return pauta;
    }

    public void setPauta(PautaDTO pauta) {
        this.pauta = pauta;
    }

    public Integer getMinutosLimite() {
        return minutosLimite;
    }

    public void setMinutosLimite(Integer minutosLimite) {
        this.minutosLimite = minutosLimite;
    }

    public LocalDateTime getDataLimite() {
        return dataLimite;
    }

    public void setDataLimite(LocalDateTime dataLimite) {
        this.dataLimite = dataLimite;
    }


}
