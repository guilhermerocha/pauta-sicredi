package com.guilherme.pautasicredi.dto;

public class PautaRequestDTO {
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
