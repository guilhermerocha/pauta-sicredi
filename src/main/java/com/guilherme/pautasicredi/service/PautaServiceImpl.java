package com.guilherme.pautasicredi.service;

import com.guilherme.pautasicredi.dto.PautaDTO;
import com.guilherme.pautasicredi.dto.PautaRequestDTO;
import com.guilherme.pautasicredi.model.Pauta;
import com.guilherme.pautasicredi.repository.PautaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PautaServiceImpl implements PautaService {

    private final PautaRepository pautaRepository;

    @Autowired
    public PautaServiceImpl(PautaRepository pautaRepository) {
        this.pautaRepository = pautaRepository;
    }

    @Override
    public PautaDTO criaPauta(PautaRequestDTO dto) {
        Pauta pauta = new Pauta(dto.getNome());
        pauta = pautaRepository.insert(pauta);
        return pauta.buildPautaToDTO();
    }

    @Override
    public List<PautaDTO> obterPautas() {
        List<Pauta> pautasCadastradas = pautaRepository.findAll();
        return pautasCadastradas
                .stream()
                .map(Pauta::buildPautaToDTO)
                .collect(Collectors.toList());
    }
}
