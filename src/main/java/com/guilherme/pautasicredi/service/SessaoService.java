package com.guilherme.pautasicredi.service;

import com.guilherme.pautasicredi.dto.AbrirSessaoDTO;
import com.guilherme.pautasicredi.dto.SessaoDTO;
import com.guilherme.pautasicredi.dto.SessaoResultadoDTO;
import com.guilherme.pautasicredi.dto.VotoDTO;
import com.guilherme.pautasicredi.model.Sessao;

import java.util.List;

public interface SessaoService {

    SessaoDTO criarUmaSessaoDeVotacao(AbrirSessaoDTO abrirSessaoDTO);

    List<SessaoDTO> obterListaDeSessoesParaVotar();

    SessaoDTO obterSessoesPorID(String idSessao);

    Sessao obterSessao(String idSessao);

    SessaoResultadoDTO obtemResultadoVotacao(String id);

    void votarNaPauta(VotoDTO votoDTO);
}
