package com.guilherme.pautasicredi.service;

import com.guilherme.pautasicredi.dto.AbrirSessaoDTO;
import com.guilherme.pautasicredi.dto.SessaoDTO;
import com.guilherme.pautasicredi.dto.SessaoResultadoDTO;
import com.guilherme.pautasicredi.dto.VotoDTO;
import com.guilherme.pautasicredi.exception.BusinessException;
import com.guilherme.pautasicredi.exception.NotFoundException;
import com.guilherme.pautasicredi.model.Pauta;
import com.guilherme.pautasicredi.model.Resposta;
import com.guilherme.pautasicredi.model.Sessao;
import com.guilherme.pautasicredi.model.Voto;
import com.guilherme.pautasicredi.repository.PautaRepository;
import com.guilherme.pautasicredi.repository.SessaoRepository;
import com.guilherme.pautasicredi.service.integracao.IntegrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SessaoServiceImpl implements SessaoService {

    private final PautaRepository pautaRepository;
    private final SessaoRepository sessaoRepository;
    private final Environment environment;
    private final IntegrationService integration;


    @Autowired
    public SessaoServiceImpl(PautaRepository pautaRepository, SessaoRepository sessaoRepository, Environment environment, IntegrationService integration) {
        this.pautaRepository = pautaRepository;
        this.sessaoRepository = sessaoRepository;
        this.environment = environment;
        this.integration = integration;
    }

    @Override
    public SessaoDTO criarUmaSessaoDeVotacao(AbrirSessaoDTO abrirSessaoDTO) {
            Pauta pauta = this.pautaRepository.findById(abrirSessaoDTO.getIdPauta())
                            .orElseThrow(() -> new NotFoundException(MessageFormat.format("Não foi encontrado uma pauta de id: ", abrirSessaoDTO.getIdPauta())));

            Integer minutosLimite = abrirSessaoDTO.getMinutosLimite();
            if (minutosLimite == null || minutosLimite <= 0) {
                minutosLimite = (Integer.parseInt(Optional
                        .ofNullable(environment.getProperty("default.limite.minutos"))
                        .orElseThrow(() -> new BusinessException("Ocorreu um problema interno."))));
            }
            Sessao sessao = new Sessao(pauta, minutosLimite,  LocalDateTime.now().plusSeconds(minutosLimite * 60), new ArrayList<>());
            return this.sessaoRepository.insert(sessao).entityToDTO();
    }

    @Override
    public List<SessaoDTO> obterListaDeSessoesParaVotar(){
        return this.sessaoRepository.findAll()
                .stream()
                .map(Sessao::entityToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public SessaoDTO obterSessoesPorID(final String idSessao){
        return this.obterSessao(idSessao).entityToDTO();

    }

    public Sessao obterSessao(String idSessao) {
        return this.sessaoRepository.findById(String.valueOf(idSessao))
                .orElseThrow(() -> new BusinessException(MessageFormat.format("Não foi encontrado sessão para o id: ", idSessao)));
    }

    @Override
    public SessaoResultadoDTO obtemResultadoVotacao(String id) {
        Sessao sessao = this.obterSessao(id);

        if (!sessao.sessaoFechada()){
            throw new BusinessException("A votação ainda está em andamento.");
        }

        List<Voto> votos = sessao.getVotos();

        return  new SessaoResultadoDTO(
                votos.stream().filter(voto -> voto.getResposta().equals(Resposta.SIM)).count(),
                votos.stream().filter(voto -> voto.getResposta().equals(Resposta.NAO)).count(),
                sessao.entityToDTO().getPauta()
        );
    }

    @Override
    public void votarNaPauta(VotoDTO votoDTO) {
        Sessao sessao = this.obterSessao(votoDTO.getSessaoId());
        if (sessao.sessaoFechada()) {
            throw new BusinessException("A sessão está fechada.");
        }
        if (sessao.cpfVotouNaPauta(votoDTO.getCpf())) {
            throw new BusinessException("O CPF {0} já fez o voto na pauta.");
        }
//        if (integration.isAbleToVote(votoDTO.getCpf())){
//            throw new BusinessException("CPF Inválido.");
//        }
        sessao.votar(new Voto(votoDTO.getCpf(),votoDTO.getResposta()));
        this.sessaoRepository.save(sessao);
    }
}
