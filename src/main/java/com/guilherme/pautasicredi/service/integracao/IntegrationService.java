package com.guilherme.pautasicredi.service.integracao;

import com.guilherme.pautasicredi.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Service
public class IntegrationService {

    private RestTemplate restTemplate;
    private final String ABLE_TO_VOTE = "ABLE_TO_VOTE";

    @Autowired
    public IntegrationService( RestTemplateBuilder restTemplate) {
        this.restTemplate = restTemplate.build();
    }

    public boolean isAbleToVote(String cpf) {
        try {
            String url = "https://user-info.herokuapp.com/users/".concat(cpf);
            ResponseEntity<?> response = restTemplate.getForEntity(url, String.class);

            return Objects.requireNonNull(response.getBody()).equals(ABLE_TO_VOTE);
        } catch (HttpStatusCodeException ex) {
            if (ex.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new BusinessException("CPF Inválido!");
            } else {
                throw new BusinessException("Erro interno no servidor!");
            }
        }
    }
}
