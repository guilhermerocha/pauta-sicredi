package com.guilherme.pautasicredi.service;

import com.guilherme.pautasicredi.dto.PautaDTO;
import com.guilherme.pautasicredi.dto.PautaRequestDTO;

import java.util.List;

public interface PautaService {

    PautaDTO criaPauta(PautaRequestDTO dto);
    List<PautaDTO> obterPautas();
}
