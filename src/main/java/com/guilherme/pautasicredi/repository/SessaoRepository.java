package com.guilherme.pautasicredi.repository;

import com.guilherme.pautasicredi.model.Pauta;
import com.guilherme.pautasicredi.model.Sessao;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SessaoRepository extends MongoRepository<Sessao, String> {

}
