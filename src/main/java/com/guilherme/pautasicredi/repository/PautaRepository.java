package com.guilherme.pautasicredi.repository;

import com.guilherme.pautasicredi.model.Pauta;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PautaRepository extends MongoRepository<Pauta, String> {
}
