package com.guilherme.pautasicredi.controller;

import com.guilherme.pautasicredi.dto.*;
import com.guilherme.pautasicredi.service.SessaoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(value = "sessao", produces = "application/json")
public class SessaoController {

    @Autowired
    private final SessaoService service;

    public SessaoController(SessaoService service) {
        this.service = service;
    }

    @ApiOperation(value="Abre uma nova sessão de votação", response = PautaDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Sessão aberta com sucesso.")
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping()
    public ResponseEntity<SessaoDTO> abrirSessao(@RequestBody() AbrirSessaoDTO dto) throws URISyntaxException {
        SessaoDTO retorno = this.service.criarUmaSessaoDeVotacao(dto);
        return ResponseEntity
                .created(new URI(retorno.getId()))
                .body(retorno);
    }

    @ApiOperation(value="Obtém as sessões", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sessões encontradas.")
    })
    @GetMapping()
    public ResponseEntity<List<SessaoDTO>> obterSessoes() {
        List<SessaoDTO> retorno = this.service.obterListaDeSessoesParaVotar();
        return ResponseEntity.ok(retorno);
    }

    @ApiOperation(value="Obtém sessão por ID da sessão", response = SessaoDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sessão encontrada.")
    })
    @GetMapping("/{idsessao}")
    public ResponseEntity<SessaoDTO> obterSessaoPorId(@PathVariable() String idsessao) throws URISyntaxException {
        SessaoDTO retorno = this.service.obterSessoesPorID(idsessao);
        return ResponseEntity
                .created(new URI(retorno.getId()))
                .body(retorno);
    }

    @ApiOperation(value="Vota em uma pauta")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Voto registrado.")
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/votar")
    public ResponseEntity<?> registrarVoto(@RequestBody() VotoDTO dto) throws URISyntaxException {
        this.service.votarNaPauta(dto);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value="Obtém resultado dos votos", response = SessaoResultadoDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Resultados.")
    })
    @GetMapping("/resultado/{idsessao}")
    public ResponseEntity<SessaoResultadoDTO> obterResultadoDaPauta(@PathVariable() String idsessao) throws URISyntaxException {
        SessaoResultadoDTO retorno = this.service.obtemResultadoVotacao(idsessao);
        return ResponseEntity
                .created(new URI(retorno.getPauta().getId()))
                .body(retorno);
    }
}
