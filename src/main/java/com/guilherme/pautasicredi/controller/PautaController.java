package com.guilherme.pautasicredi.controller;

import com.guilherme.pautasicredi.dto.PautaDTO;
import com.guilherme.pautasicredi.dto.PautaRequestDTO;
import com.guilherme.pautasicredi.service.PautaService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(value = "pauta", produces = "application/json")
public class PautaController {

    @Autowired
    private final PautaService service;

    public PautaController(PautaService service) {
        this.service = service;
    }

    @ApiOperation(value="Cadastra uma pauta", response = PautaDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Pauta criada com sucesso.")
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping()
    public ResponseEntity<PautaDTO> cadastraUmaPauta(@RequestBody PautaRequestDTO pautaDTO) throws URISyntaxException {
        PautaDTO body = this.service.criaPauta(pautaDTO);
        return ResponseEntity
                .created(new URI(body.getId()))
                .body(body);
    }

    @ApiOperation(value="Obtém todas as pautas.", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Pauta encontrada.")
    })
    @GetMapping()
    public ResponseEntity<List<PautaDTO>> obtemTodasAsPautasCadastradas()  {
        return ResponseEntity.ok(this.service.obterPautas());
    }
}
