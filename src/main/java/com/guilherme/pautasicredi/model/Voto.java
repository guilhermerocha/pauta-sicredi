package com.guilherme.pautasicredi.model;

public class Voto {

    private String cpf;
    private Resposta resposta;

    public Voto(String cpf, Resposta resposta) {
        this.cpf = cpf;
        this.resposta = resposta;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Resposta getResposta() {
        return resposta;
    }

    public void setResposta(Resposta resposta) {
        this.resposta = resposta;
    }
}
