package com.guilherme.pautasicredi.model;

import com.guilherme.pautasicredi.dto.PautaDTO;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "pauta")
public class Pauta {

    @Id
    private String id;
    private String nome;

    public Pauta() {
    }

    public Pauta(String nome) {
        this.nome = nome;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public PautaDTO buildPautaToDTO(){
        PautaDTO pauta = new PautaDTO();
        pauta.setId(getId());
        pauta.setNome(getNome());
        return pauta;
    }
}
