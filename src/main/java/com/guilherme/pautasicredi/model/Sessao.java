package com.guilherme.pautasicredi.model;

import com.guilherme.pautasicredi.dto.SessaoDTO;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document(collection = "sessao")
public class Sessao {

    @Id
    private String id;

    private Pauta pauta;

    private Integer minutosLimite;

    private LocalDateTime dataLimite;

    private List<Voto> votos;

    private boolean fechado;

    public Sessao(Pauta pauta, Integer minutosLimite, LocalDateTime dataLimite, List<Voto> votos) {
        this.pauta = pauta;
        this.minutosLimite = minutosLimite;
        this.dataLimite = dataLimite;
        this.votos = votos;
    }

    public Sessao() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Pauta getPauta() {
        return pauta;
    }

    public void setPauta(Pauta pauta) {
        this.pauta = pauta;
    }

    public Integer getMinutosLimite() {
        return minutosLimite;
    }

    public void setMinutosLimite(Integer minutosLimite) {
        this.minutosLimite = minutosLimite;
    }

    public LocalDateTime getDataLimite() {
        return dataLimite;
    }

    public void setDataLimite(LocalDateTime dataLimite) {
        this.dataLimite = dataLimite;
    }

    public List<Voto> getVotos() {
        return votos;
    }

    public void setVotos(List<Voto> votos) {
        this.votos = votos;
    }

    public void votar(Voto voto){
        this.votos.add(voto);
    }
    public boolean isFechado() {
        return fechado;
    }

    public void setFechado(boolean fechado) {
        this.fechado = fechado;
    }

    public  SessaoDTO entityToDTO(){
        SessaoDTO dto = new SessaoDTO();
        dto.setDataLimite(getDataLimite());
        dto.setPauta(getPauta().buildPautaToDTO());
        dto.setId(getId());
        dto.setMinutosLimite(getMinutosLimite());
        return dto;
    }

    public boolean sessaoFechada() {
        return this.getDataLimite().isBefore(LocalDateTime.now());
    }

    public boolean cpfVotouNaPauta(String cpf) {
        return this.votos.stream().anyMatch(vote -> vote.getCpf().equals(cpf));
    }
}
