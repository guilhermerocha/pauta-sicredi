package com.guilherme.pautasicredi.model;

public enum Resposta {
    NAO("Não"),
    SIM("Sim");

    private String resposta;

    Resposta(String resposta) {
        this.resposta = resposta;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }
}
